<?php
function residentialSaleHouse($column){
    // [0] => ID 
    // [1] => Category 
    // [2] => Sub Category 
    // [3] => Deal Name 
    // [4] => Type 
    // [5] => Income 
    // [6] => Currency 
    // [7] => Property Size in Perches 
    // [8] => Building / Apartment Size in Sq Ft 
    // [9] => Location - City 
    //[10] => Bedrooms 
    // [11] => Bathrooms 
    // [12] => Furnished 
    // [13] => Parking Space 
    // [14] => Garden 
    // [15] => Pool 
    // [16] => Gym 
    // [17] => Google Drive Link to Images )
    $post_title = $column[3];
    global $wpdb;
    $wpdb->insert('wp_posts', array(
        'post_title' => $post_title,
        'post_author' => '1',
        'post_type' => 'properties',
        'post_status' => 'publish',
        'post_date'   => current_time('mysql', 1),
        'post_date_gmt'   => current_time('mysql', 1),
    ));
    $post_id = $wpdb->insert_id;

    $property_type_key = 'wp_rem_property_type';
    $property_type_value = 'residential-for-sale';
    update_post_meta($post_id, $property_type_key, $property_type_value);

    $property_category_key = 'wp_rem_property_category';
    $property_category_value = 'residential-house-forsale';
    $property_category = array();
    $property_category['parent'] = $property_category_value;
    update_post_meta($post_id, $property_category_key, $property_category);

    $property_price_option_key = 'wp_rem_property_price_options';
    $property_price_option_value = 'price';
    update_post_meta($post_id, $property_price_option_key, $property_price_option_value);
    
    $property_currency_type_key = 'wp_rem_property_currency_type';
    $property_currency_type_value = 'lkr';

    if(trim($column[4]) == 'US Dollar'){ //US Dollar, Sri Lankan Rupee
        $property_currency_type_value = 'usd';
    }else if(trim($column[4]) == 'Sri Lankan Rupee'){
        $property_currency_type_value = 'lkr';
    }
    update_post_meta($post_id, $property_currency_type_key, $property_currency_type_value);

    $property_price_key = 'wp_rem_property_price';
    $property_price_value = trim($column[7]);
    update_post_meta($post_id, $property_price_key, $property_price_value);

    $property_status_key = 'wp_rem_property_status';
    $property_status_value = 'awaiting-activation';//awaiting-activation/active/inactive
    update_post_meta($post_id, $property_status_key, $property_status_value);

    //Location
    $location_country_key = 'wp_rem_post_loc_country_property';
    $location_country_value = 'sri-lanka';
    update_post_meta($post_id, $location_country_key, $location_country_value);
    
    $location_state_key = 'wp_rem_post_loc_state_property';
    $location_state_value = 'western-province';
    update_post_meta($post_id, $location_state_key, $location_state_value);
    $city = trim($column[9]);
    $location_city_value = 'colombo-01';//colombo-01/colombo-02
    if($city == 'Colombo 1: Fort'){//colombo-01
        $location_city_value = 'colombo-01';//colombo-01/colombo-02
    }else if($city == 'Colombo 5: Narahenpita, Havelock Town , Kirulapone'){//colombo-05
        $location_city_value = 'colombo-05';//colombo-01/colombo-02
    }
    $location_city_key = 'wp_rem_post_loc_city_property';
    $location_city_value = 'colombo-01';//colombo-01/colombo-02
    update_post_meta($post_id, $location_city_key, $location_city_value);

    $property_bedrooms_key = 'min-beds';//wp_rem_cus_field[min-beds]
    $property_bedrooms_value = trim($column[10]);
    update_post_meta($post_id, $property_bedrooms_key, $property_bedrooms_value);
    $property_bathrooms_key = 'min-bath';//wp_rem_cus_field[min-bath]
    $property_bathrooms_value = trim($column[11]);
    update_post_meta($post_id, $property_bathrooms_key, $property_bathrooms_value);

    $property_parking_key = 'min-garage';//wp_rem_cus_field[min-garage]
    $property_parking_value = trim($column[13]);
    update_post_meta($post_id, $property_parking_key, $property_parking_value);    

    $property_prop_id_key = 'proprty-id';//wp_rem_cus_field[proprty-id]
    $property_prop_id_value = trim($column[0]);
    update_post_meta($post_id, $property_prop_id_key, $property_prop_id_value);
    
    $property_furnished_key  = 'property-furnished';//wp_rem_cus_field[property-furnished]
    $property_furnished_value = 'furnished';//furnished/unfurnished
    update_post_meta($post_id, $property_furnished_key, $property_furnished_value);

    $property_sqft_key = 'area-ft';//wp_rem_cus_field[area-ft]
    $property_sqft_value = trim($column[8]);
    update_post_meta($post_id, $property_sqft_key, $property_sqft_value);

    $property_features_list = array();
    $property_features_list = get_post_meta($post_id, 'wp_rem_property_feature_list', true);

    $property_maids_room   = 'Maid\'s Room_icon_icondefault';
    $property_maids_bathroom    = 'Maid\'s Bedroom_iconicon-double-king-size-bed_icondefault';
    $property_pool    = 'Swimming Pool_iconicon-pool_icondefault';
    $property_gym    = 'Gym_iconicon-man_icondefault';
    $property_visitor_parking     = 'Visitor Parking Facility Available_iconicon-local_parking_icondefault';
    $property_clubhouse = 'Club House_icon_icondefault';
    $property_garden = 'Garden_iconicon-tree_icondefault';

    //features list
    $property_features_list = array();
    if(trim($column[14]) == 'yes'){//Garden 
        $property_features_list[] = $property_garden;
    } 
    if(trim($column[15]) == 'yes'){//Pool 
        $property_features_list[] = $property_pool;
    }
    if(trim($column[16]) == 'yes'){//Gym 
        $property_features_list[] = $property_gym;
    }

    update_post_meta($post_id, 'wp_rem_property_feature_list', $property_features_list);
    $property_posted_key = 'wp_rem_property_posted';
    $property_posted_value = '09-02-2017';// dd-mm-yyyy
    update_post_meta($post_id, $property_posted_key, $property_posted_value);
    $property_expired_key = 'wp_rem_property_expired';
    $property_expired_value = '30-09-2021';// dd-mm-yyyy
    update_post_meta($post_id, $property_expired_key, $property_expired_value);
}
?>