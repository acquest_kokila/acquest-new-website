<?php
function commercialBuy($column){
// Array ( 
//     [0] => ID 
//     [1] => Category 
//     [2] => Deal Name 
//     [3] => Type 
//     [4] => Income 
//     [5] => Currency 
//     [6] => Property Size in Perches 
//     [7] => Building / Apartment Size in Sq Ft 
//     [8] => Location - City 
//     [9] => Parking Space
//     [10] => Main Road Frontage (facing in Ft.) 
//     [11] => Backup Generator 
//     [12] => Property extent (Perches) 
//     [13] => Access road width ft. 
//     [14] => Backup power 
//     [15] => A/C 
//     [16] => Elevator 
//     [17] => Type Of Building 
//     [18] => Property Grading 
//     [19] => Mall Space 
//     [20] => Suitable for F&B 
//     [21] => Suitable for mix use 
//     [22] => Service crew 
//     [23] => 40 ft Container Access 
//     [24] => Smart Warehouse 
//     [25] => Refrigeration facility 
//     [26] => Refrigeration facility
//         )
    $post_title = $column[2];
    global $wpdb;
    $wpdb->insert('wp_posts', array(
        'post_title' => $post_title,
        'post_author' => '1',
        'post_type' => 'properties',
        'post_status' => 'publish',
        'post_date'   => current_time('mysql', 1),
        'post_date_gmt'   => current_time('mysql', 1),
    ));
    $post_id = $wpdb->insert_id;

    $property_type_key = 'wp_rem_property_type';
    $property_type_value = 'commercial-buy';
    update_post_meta($post_id, $property_type_key, $property_type_value);

    $property_category_key = 'wp_rem_property_category';
    $property_cat = trim($column[17]);

    if($property_cat == 'Building'){
        $property_category_value = 'commercial-buildings-forsale';
    }
    // else if($property_cat == 'Office Space'){
    //     $property_category_value = 'residential-house-forsale';
    // }
    else if($property_cat == 'Retail/Showroom Space'){
        $property_category_value = 'commercial-retail-forsale';
    }else if($property_cat == 'Land'){
        $property_category_value = 'commercial-land-forsale';
    }
    // else if($property_cat == 'Serviced Office Space'){
    //     $property_category_value = 'residential-house-forsale';
    // }
    else if($property_cat == 'Warehouses'){
        $property_category_value = 'commercial-warehouse-forsale';
    }else if($property_cat == 'Factories'){
        $property_category_value = 'commercial-factories-forsale';
    }

    $property_category = array();
    $property_category['parent'] = $property_category_value;
    if($property_category_value != null){
        update_post_meta($post_id, $property_category_key, $property_category);
    }

    $property_price_option_key = 'wp_rem_property_price_options';
    $property_price_option_value = 'price';
    update_post_meta($post_id, $property_price_option_key, $property_price_option_value);
    
    $property_currency_type_key = 'wp_rem_property_currency_type';
    $property_currency_type_value = 'lkr';

    if(trim($column[5]) == 'US Dollar'){ //US Dollar, Sri Lankan Rupee
        $property_currency_type_value = 'usd';
    }else if(trim($column[5]) == 'Sri Lankan Rupee'){
        $property_currency_type_value = 'lkr';
    }
    update_post_meta($post_id, $property_currency_type_key, $property_currency_type_value);

    $property_price_key = 'wp_rem_property_price';
    $property_price_value = trim($column[4]);
    update_post_meta($post_id, $property_price_key, $property_price_value);

    $property_status_key = 'wp_rem_property_status';
    $property_status_value = 'awaiting-activation';//awaiting-activation/active/inactive
    update_post_meta($post_id, $property_status_key, $property_status_value);

    //Location
    $location_country_key = 'wp_rem_post_loc_country_property';
    $location_country_value = 'sri-lanka';
    update_post_meta($post_id, $location_country_key, $location_country_value);
    
    $location_state_key = 'wp_rem_post_loc_state_property';
    $location_state_value = 'western-province';
    update_post_meta($post_id, $location_state_key, $location_state_value);
    $city = trim($column[8]);
    $location_city_value = 'colombo-01';//colombo-01/colombo-02
    if($city == 'Colombo 1: Fort'){//colombo-01
        $location_city_value = 'colombo-01';//colombo-01/colombo-02
    }else if($city == 'Colombo 5: Narahenpita, Havelock Town , Kirulapone'){//colombo-05
        $location_city_value = 'colombo-05';//colombo-01/colombo-02
    }
    $location_city_key = 'wp_rem_post_loc_city_property';
    $location_city_value = 'colombo-01';//colombo-01/colombo-02
    update_post_meta($post_id, $location_city_key, $location_city_value);


    $property_parking_key = 'min-garage';//wp_rem_cus_field[min-garage]
    $property_parking_value = trim($column[13]);
    update_post_meta($post_id, $property_parking_key, $property_parking_value);    

    $property_prop_id_key = 'proprty-id';//wp_rem_cus_field[proprty-id]
    $property_prop_id_value = trim($column[0]);
    update_post_meta($post_id, $property_prop_id_key, $property_prop_id_value);
    
    $property_furnished_key  = 'property-furnished';//wp_rem_cus_field[property-furnished]
    $property_furnished_value = 'furnished';//furnished/unfurnished
    update_post_meta($post_id, $property_furnished_key, $property_furnished_value);

    $property_sqft_key = 'sq-ft';//wp_rem_cus_field[area-ft]
    $property_sqft_value = trim($column[7]);
    update_post_meta($post_id, $property_sqft_key, $property_sqft_value);

    $property_features_list = array();
    $property_features_list = get_post_meta($post_id, 'wp_rem_property_feature_list', true);

    $property_main_road_frontage = 'Main Road Frontage_iconicon-road_icondefault';
    $property_40ft_road = '40 ft Access Road_iconicon-road2_icondefault';
    $property_backup_power = 'Back-up Power_iconicon-power_icondefault';
    $property_a_grade_building = 'A Grade Building_iconicon-office_icondefault';
    $property_b_grade_building = 'B Grade Building_iconicon-office_icondefault';
    $property_c_grade_building = 'C Grade Building_iconicon-office_icondefault';
    $property_elevator = 'Elevator_icon_icondefault';
    $property_sutable_fb = 'Suitable for F&B_iconicon-spoon-knife_icondefault';
    $property_sutable_mxed_use = 'Suitable for Mixed-Use_icon_icondefault';
    $property_mall_space = 'Mall Space_iconicon-local_mall_icondefault';
    $property_service_crew = 'Service Crew_iconicon-users2_icondefault';
    $property_40ft_contained_access = '40 ft Contained Access_icon_icondefault';
    $property_smart_warehouse = 'Smart Warehouse_icon_icondefault';
    $property_refrigeration = 'Refrigeration Facilities_icon_icondefault';

    //features list
    $property_features_list = array();
    if(trim($column[10]) == 'yes'){//Main Road Frontage 
        $property_features_list[] = $property_main_road_frontage;
    } 
    if(trim($column[14]) == 'yes'){//Backup power 
        $property_features_list[] = $property_backup_power;
    }
    if(trim($column[16]) == 'yes'){//Elevator 
        $property_features_list[] = $property_elevator;
    }
    if(trim($column[16]) == 'yes'){//Elevator 
        $property_features_list[] = $property_elevator;
    }
    if(trim($column[18]) == 'A'){
        $property_features_list[] = $property_a_grade_building;
    }else if(trim($column[18]) == 'B'){
        $property_features_list[] = $property_b_grade_building;
    }else if(trim($column[18]) == 'C'){
        $property_features_list[] = $property_c_grade_building;
    }

    if(trim($column[20]) == 'yes'){
        $property_features_list[] = $property_sutable_fb;
    }
    if(trim($column[21]) == 'yes'){
        $property_features_list[] = $property_sutable_mxed_use;
    }
    if(trim($column[19]) == 'yes'){
        $property_features_list[] = $property_mall_space;
    }
    if(trim($column[22]) == 'yes'){
        $property_features_list[] = $property_service_crew;
    }
    if(trim($column[23]) == 'yes'){
        $property_features_list[] = $property_40ft_contained_access;
    }
    if(trim($column[24]) == 'yes'){
        $property_features_list[] = $property_smart_warehouse;
    }
    if(trim($column[25]) == 'yes'){
        $property_features_list[] = $property_refrigeration;
    }
    
    update_post_meta($post_id, 'wp_rem_property_feature_list', $property_features_list);
    $property_posted_key = 'wp_rem_property_posted';
    $property_posted_value = '09-02-2017';// dd-mm-yyyy
    update_post_meta($post_id, $property_posted_key, $property_posted_value);
    $property_expired_key = 'wp_rem_property_expired';
    $property_expired_value = '30-09-2021';// dd-mm-yyyy
    update_post_meta($post_id, $property_expired_key, $property_expired_value);
}
?>