<?php
/*
Plugin Name: Import Property CSV
Plugin URI: www.acquest.lk
description: A plugin to import property CSV
Version: 1.0
Author: Kokila
Author URI: www.acquest.lk
License: GPL2
*/

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

// create custom plugin settings menu
add_action('admin_menu', 'importcsv_plugin_create_menu');

define('CSV_PATH',dirname(__FILE__));

function importcsv_plugin_create_menu() {

    //create new top-level menu
    add_menu_page('Import Property CSV Plugin Settings', 'Import Property CSV', 'administrator', __FILE__, 'importcsv_settings_page' , plugins_url('/assets/images/plugin_icon.png'),80);

    //call register settings function
//    add_action( 'admin_init', 'register_importcsv_settings' );
}

include('property-types/residential-rent-apartment.php');
include('property-types/residential-rent-house.php');
include('property-types/residential-sale-apartment.php');
include('property-types/residential-sale-house.php');
include('property-types/commercial-buy.php');

function importcsv_settings_page() {
?>
    <script type="text/javascript">
            $(document).ready(
            function() {
                $("#frmCSVImport").on(
                    "submit",
                    function() {
                        $("#response").attr("class", "");
                        $("#response").html("");
                        var fileType = ".csv";
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+("
                            + fileType + ")$");
                        if (!regex.test($("#file").val().toLowerCase())) {
                            $("#response").addClass("error");
                            $("#response").addClass("display-block");
                            $("#response").html(
                                "Invalid File. Upload : <b>" + fileType
                                + "</b> Files.");
                            return false;
                        }
                        return true;
                    });
            });

    </script>

    <h1>Import Property CSV</h1>

    <form class="form-horizontal" action="" method="post" name="uploadCSV"
          enctype="multipart/form-data">
        <div class="input-row">
            <label class="col-md-4 control-label">Choose CSV File</label> <input
                    type="file" name="file" id="file" accept=".csv">
            <select name="property-type-category">
                <option value="residentialRentApartment">Residential_Rent_Apartments</option>
                <option value="residentialRentHouse">Residential_Rent_Houses</option>
                <option value="residentialRentLand">Residential_Rent_Land</option>
                <option value="residentialSaleApartment">Residential_Buy_Apartments</option>
                <option value="residentialSaleHouse">Residential_Buy_Houses</option>
                <option value="commercialBuy">commercialBuy</option>
            </select>
            <button type="submit" id="submit" name="import"
                    class="btn-submit">Import</button>
            <br />

        </div>

        <div id="response"></div>
    </form>
<?php
}

if ( is_admin() && isset( $_POST['import'])  && isset($_POST['property-type-category']) ){
    $fileName = $_FILES["file"]["tmp_name"];
    $_prtypecat = $_POST['property-type-category'];
    $property_type_category_arr = array(
        'residentialRentApartment',
        'residentialRentHouse',
        //'residentialSaleApartment',
        'residentialSaleHouse',
        'commercialBuy',
    );
     
    if ($_FILES["file"]["size"] > 0 && in_array($_prtypecat, $property_type_category_arr)) {
       $file = fopen($fileName, "r");
        $i=0;
        while (($column = fgetcsv($file, 1000, ",")) !== FALSE) {
            if($i > 0 && $_prtypecat == 'residentialRentApartment'){
                residentialRentApartment($column);
            break;
            }
            if($i > 0 && $_prtypecat == 'residentialRentHouse'){
                residentialRentHouse($column);
            }
            if($i > 0 && $_prtypecat == 'residentialSaleApartment'){
                residentialSaleApartment($column);
            break;
            }
            if($i > 0 && $_prtypecat == 'residentialSaleHouse'){
                residentialSaleHouse($column);
            }
            if($i > 0 && $_prtypecat == 'commercialBuy'){
                commercialBuy($column);
            }
            
            $i++;
        }
        fclose($file);

    }else{
        echo  'not Valid!';
    }
}
?>