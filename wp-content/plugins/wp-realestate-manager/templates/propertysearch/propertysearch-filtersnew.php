<?php
/**
 * Property search box
 * default variable which is getting from ajax request or shotcode
 * $property_short_counter, $property_arg
 */

global $wp_rem_plugin_options, $wp_rem_form_fields_frontend, $wp_rem_post_property_types, $wp_rem_shortcode_properties_frontend, $wp_rem_search_fields,$property_type;
$property_type='residential';//residential Get this from the sessions that is set on tab change
//echo $property_type;
$propertysearch_title_switch = isset($atts['propertysearch_title_field_switch']) ? $atts['propertysearch_title_field_switch'] : '';
$propertysearch_property_type_switch = isset($atts['propertysearch_property_type_field_switch']) ? $atts['propertysearch_property_type_field_switch'] : '';
$propertysearch_location_switch = isset($atts['propertysearch_location_field_switch']) ? $atts['propertysearch_location_field_switch'] : '';
$propertysearch_categories_switch = isset($atts['propertysearch_categories_field_switch']) ? $atts['propertysearch_categories_field_switch'] : '';
$propertysearch_price_switch = isset($atts['propertysearch_price_field_switch']) ? $atts['propertysearch_price_field_switch'] : '';
$propertysearch_advance_filter_switch = isset($atts['propertysearch_advance_filter_switch']) ? $atts['propertysearch_advance_filter_switch'] : '';
$property_types_array = array();
wp_enqueue_script('bootstrap-datepicker');
wp_enqueue_style('datetimepicker');
wp_enqueue_style('datepicker');
wp_enqueue_script('datetimepicker');
$search_title = isset($_REQUEST['search_title']) ? $_REQUEST['search_title'] : '';
$property_type_slug = '';
$wp_rem_form_fields_frontend->wp_rem_form_hidden_render(
    array(
        'simple' => true,
        'cust_id' => '',
        'cust_name' => '',
        'classes' => "property-counter",
        'std' => absint($property_short_counter),
    )
);

$wp_rem_post_property_types = new Wp_rem_Post_Property_Types();
$property_types_array = $wp_rem_post_property_types->wp_rem_types_array_callback('NULL');
$key = key($property_types_array);

$property_post_name = get_page_by_path( $key, OBJECT, 'property-type' );
$property_id = $property_post_name->ID;
$wp_rem_search_result_page_type = get_post_meta($property_id, 'wp_rem_search_result_page', true);
$wp_rem_search_result_page_type = wp_rem_wpml_lang_page_permalink($wp_rem_search_result_page_type, 'page');
if ($wp_rem_search_result_page_type == ''){
    $wp_rem_search_result_page_type = $wp_rem_search_result_page;
}
?>
<div style="display:none" id='property_arg<?php echo absint($property_short_counter); ?>'><?php
    echo json_encode($property_arg);
    ?>
</div>
<form method="GET" id="top-search-form-<?php echo wp_rem_allow_special_char($property_short_counter); ?>" action="<?php echo esc_html($wp_rem_search_result_page_type); ?>" onsubmit="wp_rem_top_search('<?php echo wp_rem_allow_special_char($property_short_counter); ?>');">
    <?php echo wp_rem_wpml_lang_code_field(); ?>
    <?php if ( $popup_link_text != '' ) { ?>
        <div class="field-holder search-popup-holder">
            <a href="#" class="search-popup-btn" data-toggle="modal" data-target="#mysearchModal"><?php echo esc_html($popup_link_text); ?></a>
            <!-- Modal -->
            <div class="modal fade" id="mysearchModal" tabindex="-1" role="dialog" aria-labelledby="mysearchModalLabel" style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            <h4 class="modal-title" id="mysearchModalLabel"><?php echo wp_rem_plugin_text_srt('wp_rem_property_search_flter_wt_keyword'); ?></h4>
                        </div>
                        <div class="modal-body">
                            <p><?php echo do_shortcode($content); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div role="tabpanel" class="tab-pane" id="home">
        <div class="search-default-fields">
            <div class="search-default-fields-inner container">
            <?php if ( $propertysearch_title_switch == 'yes' ) { ?>
                <div class="field-holder search-input">
                    <label>
                        <i class="icon-search4"></i>
                        <?php
                        $wp_rem_form_fields_frontend->wp_rem_form_text_render(
                            array(
                                'cust_name' => 'search_title',
                                'classes' => 'input-field',
                                'std' => $search_title,
                                'extra_atr' => 'placeholder="' . wp_rem_plugin_text_srt('wp_rem_property_search_flter_wt_looking_for') . '"',
                            )
                        );
                        ?>
                    </label>
                </div>
                <?php
            }
            if ( $propertysearch_property_type_switch == 'yes' ) {
                ?>
                <div class="field-holder select-dropdown property-type checkbox">
                    <?php
                    $wp_rem_post_property_types = new Wp_rem_Post_Property_Types();
                    $property_types_array = $wp_rem_post_property_types->wp_rem_types_array_callback('NULL');
                    if ( is_array($property_types_array) && ! empty($property_types_array) ) {
                        foreach ( $property_types_array as $key => $value ) {
                            $property_type_slug = $key;
                           // break; set desending
                        }
                    }
                    ?>
                    <ul>
                        <?php
                        $number_option_flag = 1;
                        foreach ( $property_types_array as $key => $value ) {
                            $explode_arr = explode ("-", $key); //Get first segment of property_type slug
                            ?>
                            <li class="<?=$key?> <?=$explode_arr[0]?> ">
                                <?php
                                $property_post_name = get_page_by_path( $key, OBJECT, 'property-type' );
                                $property_id = $property_post_name->ID;
                                $wp_rem_search_result_page_type = get_post_meta($property_id, 'wp_rem_search_result_page', true);
                                $wp_rem_search_result_page_type = wp_rem_wpml_lang_page_permalink($wp_rem_search_result_page_type, 'page');
                                if ($wp_rem_search_result_page_type == ''){
                                    $wp_rem_search_result_page_type =$wp_rem_search_result_page;
                                }
                                $checked = '';
                                if ( ( (isset($_REQUEST['property_type']) && $_REQUEST['property_type'] != '') && $_REQUEST['property_type'] == $key ) || $property_type_slug == $key ) {
                                    $checked = 'checked="checked"';
                                }
                                $wp_rem_form_fields_frontend->wp_rem_form_radio_render(
                                    array(
                                        'simple' => true,
                                        'cust_id' => 'search_form_property_type' . $number_option_flag,
                                        'cust_name' => 'property_type',
                                        'std' => $key,
                                        'force_std' => true,
                                        'extra_atr' => $checked . ' onchange="wp_rem_property_type_search_fields(this,\'' . $property_short_counter . '\',\'' . $propertysearch_price_switch . '\',\''. $wp_rem_search_result_page_type . '\'); wp_rem_property_type_cate_fields(this,\'' . $property_short_counter . '\',\'' . $propertysearch_categories_switch . '\'); "',
                                    )
                                );
                                ?>
                                <label for="<?php echo force_balance_tags('search_form_property_type' . $number_option_flag) ?>"><?php echo force_balance_tags($value); ?></label>
                                <?php ?>
                            </li>
                            <?php
                            $number_option_flag ++;
                        }
                        ?>
                    </ul>
                </div>
            <?php } ?>
            <?php
            if ($propertysearch_location_switch == 'yes') {
                $wp_rem_select_display = 1;
               do_action('homevillas_search_location_filters', '', '', false, $property_short_counter, 'modern-v2');

            }            ?>
           <!-- <div id="property_type_currency_fields_<?php //echo wp_rem_allow_special_char($property_short_counter); ?>" class="property_currency_type-fields field-holder select-dropdown has-icon">

               <label>

<?php

                //    $wp_rem_opt_array = array(

                //        'std' => '',

                //        'id' => 'property_currency_type',

                //        'classes' => 'chosen-select-no-single',

                //        'cust_name' => 'property_currency_type',

                //        'options' => array(

                //            'lkr' => wp_rem_plugin_text_srt('wp_rem_list_meta_lkr'),

                //            'usd' => wp_rem_plugin_text_srt('wp_rem_list_meta_usd'),                        ),

                //    );

                //    $wp_rem_form_fields_frontend->wp_rem_form_select_render($wp_rem_opt_array);

                   ?>

               </label>

</div> -->
                <?php
                global $wpdb;
                $curreny_price_meta_key = '';
                if($_SESSION['currency'] == 'lkr'){
                    $curreny_price_meta_key = 'wp_rem_property_lkr_price';
                }elseif($_SESSION['currency'] == 'usd'){
                    $curreny_price_meta_key = 'wp_rem_property_usd_price';
                }
                $minprice_value = $wpdb->get_results("SELECT min(cast(meta_value as unsigned)) as minprices FROM wp_postmeta WHERE (meta_key='".$curreny_price_meta_key."') AND meta_value IS NOT NULL 
              AND meta_value <> 0");
                $arraym = json_decode(json_encode($minprice_value), true);

                $maxprice_value = $wpdb->get_results("SELECT max(cast(meta_value as unsigned)) as maxprices FROM wp_postmeta WHERE (meta_key='".$curreny_price_meta_key."') AND meta_value IS NOT NULL 
              AND meta_value <> 0");
                $arrayp = json_decode(json_encode($maxprice_value), true);
                ?>
                <div class="rang-text" style="padding-top: 20px;width: 13%; text-align: right; display: inline-block;">Price Range</div>
                <!-- lkr price range -->
                <?php if($_SESSION['currency'] == 'lkr'){?>
                <div class="field-holder field-range" style="width: 100%;">
                    <div class="price-per-person">
                        <input type="hidden" id="range-hidden-price-lkr" class="price-range" name="price-range" value="">
                        <input type="text" id="price-range-id" style="display: none;" data-value="0,100000000" value="0,100000000">
                        <input id="price_minimum" type="hidden" name="price_minimum"/>
                        <input id="price_maximum" type="hidden" name="price_maximum"/>
                    </div>
                    <script type="text/javascript">
                        jQuery(window).load(function(){
                            if (jQuery("#range-hidden-price-lkr").length > 0) {
                                jQuery("#range-hidden-price-lkr").slider({
                                    step : 1000,
                                    min: <?php echo $arraym[0]['minprices']; ?>,
                                    max: <?php echo $arrayp[0]['maxprices']; ?>,
                                    value: [ <?php echo $arraym[0]['minprices']; ?>,<?php echo $arrayp[0]['maxprices']; ?>],
                                });
                                jQuery("#range-hidden-price-lkr").on("slideStop", function () {
                                    var rang_slider_val = jQuery("#range-hidden-price-lkr").val();
                                    var min_max_arr = rang_slider_val.split(',');
                                    jQuery("#price_minimum").val(min_max_arr[0]);
                                    jQuery("#price_maximum").val(min_max_arr[1]);
                                    jQuery("#range-hidden-price-lkr").val(rang_slider_val);
                                });
                            }
                        });
                    </script>            
                </div>
                <?php }elseif($_SESSION['currency'] == 'usd'){ ?> 
                <!-- usd price range -->
                <div class="field-holder field-range" style="width: 100%;">
                    <div class="price-per-person">
                        <input type="hidden" id="range-hidden-price-usd" class="price-range" name="price-range" value="">
                        <input type="text" id="price-range-id" style="display: none;" data-value="0,100000000" value="0,100000000">
                        <input id="price_minimum" type="hidden" name="price_minimum"/>
                        <input id="price_maximum" type="hidden" name="price_maximum"/>
                    </div>
                    <script type="text/javascript">
                        jQuery(window).load(function(){
                            if (jQuery("#range-hidden-price-usd").length > 0) {
                                jQuery("#range-hidden-price-usd").slider({
                                    step : 1000,
                                    min: <?php echo $arraym[0]['minprices']; ?>,
                                    max: <?php echo $arrayp[0]['maxprices']; ?>,
                                    value: [ <?php echo $arraym[0]['minprices']; ?>,<?php echo $arrayp[0]['maxprices']; ?>],
                                });
                                jQuery("#range-hidden-price-usd").on("slideStop", function () {
                                    var rang_slider_val = jQuery("#range-hidden-price-usd").val();
                                    var min_max_arr = rang_slider_val.split(',');
                                    jQuery("#price_minimum").val(min_max_arr[0]);
                                    jQuery("#price_maximum").val(min_max_arr[1]);
                                    jQuery("#range-hidden-price-usd").val(rang_slider_val);
                                });
                            }
                        });
                    </script>            
                </div>
                    <?php } ?>
                    <div class="dynamic-fields">
                        <!-- Commercial land-extent - start-->
                        <div class="field-holder field-range" style="width:200px;">
                            <div class="price-per-person">
                                <span class="rang-text">Land Extent</span>
                                <input type="hidden" id="range-hidden-land-extent" class="land-extent commercial-land-extent" name="land-extent" value="">
                                <input type="text" id="price-range-id1" style="display: none;" data-value="0,100000000" value="0,100000000">
                            </div>
                            <script type="text/javascript">
                                jQuery(window).load(function(){
                                    if (jQuery("#range-hidden-land-extent").length > 0) {
                                        jQuery("#range-hidden-land-extent").slider({
											step : 500,
											min: 0,
											max: 100000,
											value: [ 0,100000],
                                        });
                                        jQuery("#range-hidden-land-extent").on("slideStop", function () {
                                            var rang_slider_val = jQuery("#range-hidden-land-extent").val();
                                            jQuery("#range-hidden-land-extent").val(rang_slider_val);
                                        });
                                    }
                                });
                            </script>            
                        </div>
                        <!-- Commercial land-extent -  end-->
                        <!-- Commercial sq-ft - start-->
                        <div class="field-holder field-range" style="width:200px;">
                            <div class="price-per-person">
                                <span class="rang-text">Sq Ft</span>
                                <input type="hidden" id="range-hidden-sq-ft1" class="sq-ft commercial-sq-ft" name="sq-ft" value="">
                                <input type="text" id="price-range-id1" style="display: none;" data-value="0,100000000" value="0,100000000">
                            </div>
                            <script type="text/javascript">
                                jQuery(window).load(function(){
                                    if (jQuery("#range-hidden-sq-ft1").length > 0) {
                                        jQuery("#range-hidden-sq-ft1").slider({
											step : 500,
											min: 0,
											max: 100000,
											value: [ 0,100000],
                                        });
                                        jQuery("#range-hidden-sq-ft1").on("slideStop", function () {
                                            var rang_slider_val = jQuery("#range-hidden-sq-ft1").val();
                                            jQuery("#range-hidden-sq-ft1").val(rang_slider_val);
                                        });
                                    }
                                });
                            </script>            
                        </div>
                        <!-- Commercial sq-ft -  end-->

                    </div>
            <?php
            $property_cats_array = $wp_rem_search_fields->wp_rem_property_type_categories_options($property_type_slug);

            if ( $propertysearch_categories_switch == 'yes' && ! empty($property_cats_array) ) {
                ?>
                <div id="property_type_cate_fields_<?php echo wp_rem_allow_special_char($property_short_counter); ?>" class="property-category-fields field-holder select-dropdown has-icon">
                    <label>
                        <?php
                        // $term_id = 103;
                        // $tax_name = 'property-category';
                        // $term_child = get_term_children( $term_id, $tax_name );
                        

                        // function startWith($val, $startwith){
                        //     $len = strlen($startwith);
                        //     return substr($val,0,$len) === $startwith;
                        // }
                        // function filterTypes($property_cats_array){
                        //     $filteredArray = array_filter($property_cats_array ,function($v,$k){  
                              
                        //         $property_type = "residential"; //residential Get this from the sessions that is set on tab change
                        //         if(startWith($k,$property_type)){
                        //             return $k;
                        //         }
                        //     },ARRAY_FILTER_USE_BOTH);
                            
                        //     return $filteredArray;
                          
                        // }

                        $wp_rem_opt_array = array(
                            'std' => '',
                            'id' => 'property_category',
                            'classes' => 'chosen-select',
                            'cust_name' => 'property_category',
                            'options' => $property_cats_array,//filterTypes($property_cats_array),//array('' => 'Type','residential-apartment-forsale' => 'Residential Apartment','residential-house-forsale' => 'Residential House','residential-land-forsale' => 'Residential Land'),
                            'extra_atr' => 'onchange="onChangeProprtyCategory(this.value);"',
                        );

                        if ( count($wp_rem_opt_array) <= 6 ) {
                            $wp_rem_opt_array['classes'] = 'chosen-select-no-single';
                        }
                        $wp_rem_form_fields_frontend->wp_rem_form_select_render($wp_rem_opt_array);
                        ?>
                    </label>

                </div>
            <?php } ?>
            <div class="field-holder search-btn">
                <div class="search-btn-loader-<?php echo wp_rem_allow_special_char($property_short_counter); ?> input-button-loader">
                    <?php
                    $wp_rem_form_fields_frontend->wp_rem_form_text_render(
                        array(
                            'cust_name' => '',
                            'classes' => 'bgcolor',
                            'std' => wp_rem_plugin_text_srt('wp_rem_property_search_flter_saerch'),
                            'cust_type' => "submit",
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
        </div> 
        <!-- search-default-fields-inner -->
        <?php
        if ( $property_type_slug != '' && $propertysearch_advance_filter_switch == 'yes' ) {
            $args = array(
                'name' => $property_type_slug,
                'post_type' => 'property-type',
                'post_status' => 'publish',
                'numberposts' => 1,
            );
            $my_posts = get_posts($args);
            if ( $my_posts ) {
                $property_type_id = $my_posts[0]->ID;
            }

            $price_type = get_post_meta($property_type_id, 'wp_rem_property_type_price_type', true);
            $wp_rem_price_minimum_options = get_post_meta($property_type_id, 'wp_rem_price_minimum_options', true);
            $wp_rem_price_minimum_options = ( ! empty($wp_rem_price_minimum_options) ) ? $wp_rem_price_minimum_options : 1;
            $wp_rem_price_max_options = get_post_meta($property_type_id, 'wp_rem_price_max_options', true);
            $wp_rem_price_max_options = ( ! empty($wp_rem_price_max_options) ) ? $wp_rem_price_max_options : 50; //50000;
            $wp_rem_price_interval = get_post_meta($property_type_id, 'wp_rem_price_interval', true);
            $wp_rem_price_interval = ( ! empty($wp_rem_price_interval) ) ? $wp_rem_price_interval : 50;
            $price_type_options = array();
            $wp_rem_price_interval = (int) $wp_rem_price_interval;
            $price_counter = $wp_rem_price_minimum_options;
            $property_price_array = array();
            // gettting all values of price
            $property_price_array = wp_rem_property_price_options($wp_rem_price_minimum_options, $wp_rem_price_max_options, $wp_rem_price_interval, wp_rem_plugin_text_srt('wp_rem_search_filter_min_price'));

            $price_min = array();
            $price_max = array();
            // gettting all values of price
            $price_min = wp_rem_property_price_options($wp_rem_price_minimum_options, $wp_rem_price_max_options, $wp_rem_price_interval, wp_rem_plugin_text_srt('wp_rem_search_filter_min_price'));
            $price_max = wp_rem_property_price_options($wp_rem_price_minimum_options, $wp_rem_price_max_options, $wp_rem_price_interval, wp_rem_plugin_text_srt('wp_rem_search_filter_max_price'));

            if ( ($propertysearch_categories_switch == 'yes' ) || ($propertysearch_price_switch == 'yes' && ! empty($property_price_array)) || $propertysearch_advance_filter_switch == 'yes' ) { ?>
                <div id="property_type_fields_<?php echo wp_rem_allow_special_char($property_short_counter); ?>" class="search-advanced-fields container" style="display:none;">
                    <div class="search-advanced-field-inner container">
                    <!-- city_repeat start -->
                    <!-- <div class="city_repeat field-holder">
                        <span class="city_label">City</span>
                        <ul class="rpt_cities">
                        </ul>
                        <ul class="rpt_cities_last">
                            <li><a href="javascript:void(0);" class="add"><span>+</span></a></li>
                        </ul>
                    </div> -->

<script>
var admin_ajax_url=  '<?php echo esc_url(admin_url("admin-ajax.php"))?>';
$(document).ready(function(){
    var i = 1;
    $(document).on("click", "a.add" , function() {
        var predictionsDropDown = '<div class="wp_rem_location_autocomplete" class="city-autocomplete" style="min-height: 35px;"></div>';
        var rpt_element = '<li><input name="city['+i+']" type="text" class="rpt_city" autocomplete="off" />';
        rpt_element += '<a href="javascript:void(0);" class="remove">&times;</a>';
        rpt_element += '</li>';
        $(".rpt_cities").append(rpt_element);
        i++;
    });
    $(document).on("focus", "input.rpt_city" , function() {
        $(this).multiCityAutocomplete();
    });
    $(document).on("click", "a.remove" , function() {

        $(this).parent().remove();

    });

});
</script>
<!-- city_repeat end -->
<!-- advanced seacrh popup start -->
<script>
$(document).on("click", ".btn-advanced-search a" , function() {
    $('.btn_close_popup').remove();
    $('.main-search').addClass('search_popup_open');
    $('body').addClass('body_fixed');
    $('.main-search').append('<span class="btn_close_popup">x</span>');
});
$(document).on("click", ".btn_close_popup" , function() {
    $('.main-search').removeClass('search_popup_open');
    $('body').removeClass('body_fixed');
    $('.btn_close_popup').remove();
    $('.search-advanced-fields').hide();
});
</script>
<!-- advanced seacrh popup end -->
<script>

function onChangeProprtyCategory(selected_value) {

    //alert(selected_value);
    var residential_bedrooms = $('#wp_rem_wp_rem_min-beds').closest('.field-holder');
    var residential_bathrooms =  $('#wp_rem_wp_rem_min-bath').closest('.field-holder');
    var residential_parking = $('#wp_rem_wp_rem_min-garage').closest('.field-holder');
    var residential_sq_ft = $('.search-advanced-fields .area-ft').parent();
    var residential_furnished =  $($('#property-furnished').parent()).parent();
    var residential_prop_id = $($('#proprty-id').parent()).parent();
    var residential_land_extent = $('.search-advanced-fields .land-extent').parent();
    var residential_feature_list = $('.features-list');
    var residential_multi_city = $('.city_repeat');

    var residential_feature_swimming_pool = $('#search-features-list :input[value="Swimming Pool"]').closest('li');
    var residential_feature_darden = $('#search-features-list :input[value="Garden"]').closest('li');
    var residential_feature_gym = $('#search-features-list :input[value="Gym"]').closest('li');
    var residential_feature_maids_room = $('#search-features-list :input[value="Maid\'s Room"]').closest('li');
    var residential_feature_maids_bathroom = $('#search-features-list :input[value="Maid\'s Bathroom"]').closest('li');
    var residential_feature_available = $('#search-features-list :input[value="Visitor Parking Facility Available"]').closest('li');
    var residential_feature_clubhouse = $('#search-features-list :input[value="Club House"]').closest('li');
    var residential_feature_waterfront = $('#search-features-list :input[value="Waterfront"]').closest('li');
    var residential_feature_beachfront = $('#search-features-list :input[value="Beachfront"]').closest('li');
    var residential_feature_hills = $('#search-features-list :input[value="Hills"]').closest('li');
        
    if(selected_value == 'residential-apartment-forsale'){
        //alert('Residential Apartment');
        residential_multi_city.show();
        residential_bedrooms.show();
        residential_bathrooms.show();
        residential_land_extent.hide();
        residential_furnished.show();
        residential_parking.show();
        residential_sq_ft.show();
        residential_prop_id.hide();
        
        residential_feature_list.show();
        residential_feature_swimming_pool.show();
        residential_feature_darden.hide();
        residential_feature_gym.show();
        residential_feature_maids_room.show();
        residential_feature_maids_bathroom.show();
        residential_feature_available.show();
        residential_feature_clubhouse.show();
        residential_feature_waterfront.hide();
        residential_feature_beachfront.hide();
        residential_feature_hills.hide();

    }else if(selected_value == 'residential-house-forsale'){
        // alert('Residential House');
        residential_multi_city.show();
        residential_bedrooms.show();
        residential_bathrooms.show();
        residential_land_extent.show();
        residential_furnished.show();
        residential_parking.show();
        residential_sq_ft.show();
        residential_prop_id.hide();

        residential_feature_list.show();
        residential_feature_swimming_pool.show();
        residential_feature_darden.show();
        residential_feature_gym.show();
        residential_feature_maids_room.hide();
        residential_feature_maids_bathroom.hide();
        residential_feature_available.hide();
        residential_feature_clubhouse.hide();
        residential_feature_waterfront.hide();
        residential_feature_beachfront.hide();
        residential_feature_hills.hide();
    }else if(selected_value == 'residential-land-forsale'){
        // alert('Residential Land');
        residential_multi_city.show();
        residential_bedrooms.hide();
        residential_bathrooms.hide();
        residential_land_extent.show();
        residential_furnished.hide();
        residential_parking.hide();
        residential_sq_ft.hide();
        residential_prop_id.show();

        residential_feature_list.show();
        residential_feature_swimming_pool.hide();
        residential_feature_darden.hide();
        residential_feature_gym.hide();
        residential_feature_maids_room.hide();
        residential_feature_maids_bathroom.hide();
        residential_feature_available.hide();
        residential_feature_clubhouse.hide();

        residential_feature_waterfront.show();
        residential_feature_beachfront.show();
        residential_feature_hills.show();
    }
    if(selected_value == 'residential-apartment-forrent'){
        residential_multi_city.show();
        residential_bedrooms.show();
        residential_bathrooms.show();
        residential_land_extent.hide();
        residential_furnished.show();
        residential_parking.show();
        residential_sq_ft.show();
        residential_prop_id.hide();
        
        residential_feature_list.show();
        residential_feature_swimming_pool.show();
        residential_feature_darden.hide();
        residential_feature_gym.show();
        residential_feature_maids_room.show();
        residential_feature_maids_bathroom.show();
        residential_feature_available.show();
        residential_feature_clubhouse.show();
        residential_feature_waterfront.hide();
        residential_feature_beachfront.hide();
        residential_feature_hills.hide();
    }else if(selected_value == 'residential-house-forrent'){
        residential_multi_city.show();
        residential_bedrooms.show();
        residential_bathrooms.show();
        residential_land_extent.show();
        residential_furnished.show();
        residential_parking.show();
        residential_sq_ft.show();
        residential_prop_id.hide();

        residential_feature_list.show();
        residential_feature_swimming_pool.show();
        residential_feature_darden.show();
        residential_feature_gym.show();
        residential_feature_maids_room.hide();
        residential_feature_maids_bathroom.hide();
        residential_feature_available.hide();
        residential_feature_clubhouse.hide();
        residential_feature_waterfront.hide();
        residential_feature_beachfront.hide();
        residential_feature_hills.hide();
    }else if(selected_value == 'residential-land-forrent'){
        residential_multi_city.show();
        residential_bedrooms.hide();
        residential_bathrooms.hide();
        residential_land_extent.show();
        residential_furnished.hide();
        residential_parking.hide();
        residential_sq_ft.hide();
        residential_prop_id.show();

        residential_feature_list.show();
        residential_feature_swimming_pool.hide();
        residential_feature_darden.hide();
        residential_feature_gym.hide();
        residential_feature_maids_room.hide();
        residential_feature_maids_bathroom.hide();
        residential_feature_available.hide();
        residential_feature_clubhouse.hide();

        residential_feature_waterfront.show();
        residential_feature_beachfront.show();
        residential_feature_hills.show();
    }

    // Commercial
    var commercial_multi_city = $('.city_repeat');
    var commercial_price_per_perch = $('.price-per-perch').parent();
    var commercial_prop_id = $($('#proprty-id').parent()).parent();
    var commercial_parking = $('#wp_rem_wp_rem_parking').closest('.field-holder');
    var commercial_land_extent = $('.search-advanced-fields .land-extent').parent();
    var commercial_price_per_seat = $('.search-advanced-fields .price-per-seat').parent();
    var commercial_no_of_seat = $('.search-advanced-fields .number-of-seats').parent();

    var commercial_feature_list = $('.features-list');

    var commercial_feature_main_road_frontage = $('#search-features-list :input[value="Main Road Frontage"]').closest('li');
    var commercial_feature_30ft_road = $('#search-features-list :input[value="30 ft Access Road"]').closest('li');
    var commercial_feature_40ft_road = $('#search-features-list :input[value="40 ft Access Road"]').closest('li');
    var commercial_feature_backup_power = $('#search-features-list :input[value="Back-up Power"]').closest('li');
    var commercial_feature_a_grade = $('#search-features-list :input[value="A Grade Building"]').closest('li');
    var commercial_feature_b_grade = $('#search-features-list :input[value="B Grade Building"]').closest('li');
    var commercial_feature_c_grade = $('#search-features-list :input[value="C Grade Building"]').closest('li');
    var commercial_feature_ac = $('#search-features-list :input[value="Air Conditioning"]').closest('li');
    var commercial_feature_elevator = $('#search-features-list :input[value="Elevator"]').closest('li');
    var commercial_feature_suitable_fb = $('#search-features-list :input[value="Suitable for F&B"]').closest('li');
    var commercial_feature_suitable_mixed = $('#search-features-list :input[value="Suitable for Mixed-Use"]').closest('li');
    var commercial_feature_mall_space = $('#search-features-list :input[value="Mall Space"]').closest('li');
    var commercial_feature_service_crew = $('#search-features-list :input[value="Service Crew"]').closest('li');
    var commercial_feature_40ft_contained_access = $('#search-features-list :input[value="40 ft Contained Access"]').closest('li');
    var commercial_feature_smart_warehouse = $('#search-features-list :input[value="Smart Warehouse"]').closest('li');
    var commercial_feature_refrigeration = $('#search-features-list :input[value="Refrigeration Facilities"]').closest('li');
    var commercial_feature_cafeteria = $('#search-features-list :input[value="Cafeteria"]').closest('li');
    var commercial_feature_parking  = $('#search-features-list :input[value="Parking"]').closest('li');
    
    var commercial_feature_list = $('.features-list');

    var commercial_df_sq_ft = $('.search-default-fields .commercial-sq-ft').closest('.field-holder');
    var commercial_df_land_extent = $('.search-default-fields .commercial-land-extent').closest('.field-holder');
    // onload hide fields
    commercial_df_sq_ft.hide();
    commercial_df_land_extent.hide();

    // Commercial - For Sale
    if(selected_value == 'commercial-land-forsale'){ //land
        commercial_multi_city.show();
        commercial_price_per_perch.show();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.show();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.show();
        commercial_df_sq_ft.hide();

        commercial_feature_main_road_frontage.show();
        commercial_feature_30ft_road.show();
        commercial_feature_40ft_road.show();
        commercial_feature_backup_power.hide();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.hide();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();

    }else if(selected_value == 'commercial-buildings-forsale'){//Buildings
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.show();
        commercial_land_extent.hide();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();

        commercial_feature_main_road_frontage.show();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.show();
        commercial_feature_a_grade.show();
        commercial_feature_b_grade.show();
        commercial_feature_c_grade.show();
        commercial_feature_ac.show();
        commercial_feature_elevator.show();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();
    }else if(selected_value == 'commercial-warehouse-forsale'){//Warehouses
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.hide();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();

        commercial_feature_main_road_frontage.hide();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.hide();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.hide();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.show();
        commercial_feature_40ft_contained_access.show();
        commercial_feature_smart_warehouse.show();
        commercial_feature_refrigeration.show();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();

    }else if(selected_value == 'commercial-factories-forsale'){//Factories
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.hide();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();

        commercial_feature_main_road_frontage.hide();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.hide();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.hide();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();
    }else if(selected_value == 'commercial-retail-forsale'){//Retail/Showroom spaces 
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.hide();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();

        commercial_feature_main_road_frontage.show();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.show();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.hide();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.show();
        commercial_feature_suitable_mixed.show();
        commercial_feature_mall_space.show();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();
    }else if(selected_value == 'commercial-offices-forsale'){//Office space 
        //N/A
    }
    // Commercial - For rent
    if(selected_value == 'commercial-land-forrent'){ //land
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.show();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.show();
        commercial_df_sq_ft.hide();

        commercial_feature_main_road_frontage.show();
        commercial_feature_30ft_road.show();
        commercial_feature_40ft_road.show();
        commercial_feature_backup_power.hide();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.hide();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();
    }else if(selected_value == 'commercial-buildings-forrent'){//Buildings
        // N/A
    }else if(selected_value == 'commercial-warehouse-forrent'){//Warehouses
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.hide();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();

        commercial_feature_main_road_frontage.hide();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.hide();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.hide();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.show();
        commercial_feature_40ft_contained_access.show();
        commercial_feature_smart_warehouse.show();
        commercial_feature_refrigeration.show();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();

    }else if(selected_value == 'commercial-factories-forrent'){//Factories
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.hide();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();

        commercial_feature_main_road_frontage.hide();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.hide();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.hide();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();
    }else if(selected_value == 'commercial-retail-forrent'){//Retail/Showroom spaces 
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.hide();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();
       
        commercial_feature_main_road_frontage.show();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.show();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.hide();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.show();
        commercial_feature_suitable_mixed.show();
        commercial_feature_mall_space.show();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.hide();
        commercial_feature_parking.hide();
    }else if(selected_value == 'commercial-offices-forrent'){//Office space 
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.show();
        commercial_land_extent.hide();
        commercial_price_per_seat.hide();
        commercial_no_of_seat.hide();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();

        commercial_feature_main_road_frontage.show();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.show();
        commercial_feature_a_grade.show();
        commercial_feature_b_grade.show();
        commercial_feature_c_grade.show();
        commercial_feature_ac.show();
        commercial_feature_elevator.show();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.show();
        commercial_feature_parking.hide();
    }else if(selected_value == 'commercial-serviced-office-space-forrent'){//Serviced Office Space
        commercial_multi_city.show();
        commercial_price_per_perch.hide();
        commercial_prop_id.show();
        commercial_parking.hide();
        commercial_land_extent.hide();
        commercial_price_per_seat.show();
        commercial_no_of_seat.show();

        commercial_df_land_extent.hide();
        commercial_df_sq_ft.show();

        commercial_feature_main_road_frontage.hide();
        commercial_feature_30ft_road.hide();
        commercial_feature_40ft_road.hide();
        commercial_feature_backup_power.show();
        commercial_feature_a_grade.hide();
        commercial_feature_b_grade.hide();
        commercial_feature_c_grade.hide();
        commercial_feature_ac.show();
        commercial_feature_elevator.hide();
        commercial_feature_suitable_fb.hide();
        commercial_feature_suitable_mixed.hide();
        commercial_feature_mall_space.hide();
        commercial_feature_service_crew.hide();
        commercial_feature_40ft_contained_access.hide();
        commercial_feature_smart_warehouse.hide();
        commercial_feature_refrigeration.hide();
        commercial_feature_cafeteria.show();
        commercial_feature_parking.show();

    }
    var investments_land_extent = $('.search-advanced-fields .land-extent').parent();
    var investments_no_of_rooms = $('#wp_rem_wp_rem_number-of-rooms').closest('.field-holder');
    var investments_sq_ft = $('.search-advanced-fields .sq-ft').parent();
    var investments_crops_cultivated = $('#crops-cultivated').closest('.field-holder');

    // investments_land_extent.hide();
    // investments_no_of_rooms.hide();
    // investments_sq_ft.hide();
    // investments_crops_cultivated.hide();

    //Investments - For Sale
    if(selected_value == 'investments-hotels-forsale'){//Hotels
        investments_land_extent.show();
        investments_no_of_rooms.show();
        investments_sq_ft.hide();
        investments_crops_cultivated.hide();
    }else if(selected_value == 'investments-villas-forsale'){//Villas
        investments_land_extent.show();
        investments_no_of_rooms.show();
        investments_sq_ft.hide();
        investments_crops_cultivated.hide();
    }else if(selected_value == 'investments-industrial-forsale'){//Industrial
        investments_land_extent.show();
        investments_no_of_rooms.hide();
        investments_sq_ft.show();
        investments_crops_cultivated.hide();
    }else if(selected_value == 'investments-hotelsites-forsale'){//hotel sites
        investments_land_extent.show();
        investments_no_of_rooms.hide();
        investments_sq_ft.hide();
        investments_crops_cultivated.hide();
    }else if(selected_value == 'investments-development-sites-forsale'){//developments
        investments_land_extent.show();
        investments_no_of_rooms.hide();
        investments_sq_ft.hide();
        investments_crops_cultivated.hide();
    }else if(selected_value == 'investments-plantations-and-estates-for-sale'){//plantations and estates
        investments_land_extent.show();
        investments_no_of_rooms.hide();
        investments_sq_ft.hide();
        investments_crops_cultivated.show();
    }


    //Investments - For Rent
    if(selected_value == 'investments-hotels-forrent'){//Hotels
        investments_land_extent.show();
        investments_no_of_rooms.hide();
        investments_sq_ft.hide();
        investments_crops_cultivated.hide();
    }else if(selected_value == 'investments-villas-forrent'){//Villas
        investments_land_extent.show();
        investments_no_of_rooms.hide();
        investments_sq_ft.hide();
        investments_crops_cultivated.hide();
    }else if(selected_value == 'investments-industrial-forrent'){//Industrial
        investments_land_extent.show();
        investments_no_of_rooms.hide();
        investments_sq_ft.show();
        investments_crops_cultivated.hide();
    }

}
</script>
<script>
$(document).ready(function(){
    //onload hide all tabs except residential buy/rent
    $('.residential').show();
    $('.commercial').hide();
    $('.investments').hide();
    $('.new').hide();
    //property type text change ex:residential-buy => buy
    $('.property-type li').each(function(index, value) {
            var str = $(this).children("label").text();
            str = str.toString();
            var temp = str.toString().split("-");
            $(this).children("label").text(temp[temp.length - 1]);
    });
    //property category
    $('.chosen-drop .chosen-results li').each(function(index, value) {
        var str = $(this).text();
        str = str.toString();
        var temp = str.toString().split(" ");
        alert(temp[temp.length - 1]);
        $(this).text(temp[temp.length - 1]);
    });


    // default bar additional fields 
    // commercial
  //  var commercial_df_sq_ft =$($('.search-default-fields .commercial-sq-ft').parent()).parent();
    var commercial_df_sq_ft = $('.search-default-fields .commercial-sq-ft').closest('.field-holder');
    var commercial_df_land_extent = $('.search-default-fields .commercial-land-extent').closest('.field-holder');
    // onload hide fields
    commercial_df_sq_ft.hide();
    commercial_df_land_extent.hide();

    function showFieldsInDefaultBar(ptype){
        // var residential_sq_ft =$($('.search-default-fields .sq-ft').parent()).parent();
        // var investments_investment_value =$($('.search-default-fields .investment-value').parent()).parent();
        // residential_sq_ft.hide();
        // investments_investment_value.hide();


        if(ptype == 'investments'){
            commercial_df_sq_ft.hide();
            commercial_df_land_extent.hide();

        }else if(ptype == 'commercial'){
            commercial_df_sq_ft.show();
            commercial_df_land_extent.show();
        }
    }

$(document).on("click", ".property_type-tabs li" , function(e) {
    var str = $(this).text();
    var str = str.toString().split(" ",1);
    var temp = str.toString().toLowerCase();
    $('.'+temp+' input').first().trigger('click');
    $('.property-type ul li').hide();
    $('.property-type ul li.'+temp).show();
    //show fileds on default search bar
   showFieldsInDefaultBar(temp);
});

});

</script>
                    <?php
                    if ( $propertysearch_price_switch == 'yes' && ! empty($property_price_array) ) {
                        if ( $price_type == 'variant' ) {
                            $price_type_options = array(
                                '' => wp_rem_plugin_text_srt('wp_rem_search_fields_price_type_all'),
                                'variant_week' => wp_rem_plugin_text_srt('wp_rem_search_fields_price_type_per_week'),
                                'variant_month' => wp_rem_plugin_text_srt('wp_rem_search_fields_price_type_per_month'),
                            );

                            $price_type_options     = apply_filters( 'homevillas_variant_price_options', $price_type_options );
                            ?>
                            <div class="field-holder select-dropdown price-type">
                                <div class="select-categories">
                                    <ul>
                                        <li>
                                            <?php
                                            $price_type_checked = ( isset($_REQUEST['price_type']) && $_REQUEST['price_type'] ) ? $_REQUEST['price_type'] : '';
                                            $wp_rem_form_fields_frontend->wp_rem_form_select_render(
                                                array(
                                                    'simple' => true,
                                                    'cust_name' => 'price_type',
                                                    'std' => $price_type_checked,
                                                    'classes' => 'chosen-select-no-single',
                                                    'options' => $price_type_options,
                                                    'extra_atr' => 'onchange="wp_rem_property_content(\'' . $property_short_counter . '\');"',
                                                )
                                            );
                                            ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <!-- <div class="field-holder select-dropdown">
                            <div class="wp-rem-min-max-price">
                                <div class="select-categories">
                                    <ul>
                                        <li>
                                            <?php
                                            // $price_min_checked = ( isset($_REQUEST['price_minimum']) && $_REQUEST['price_minimum'] ) ? $_REQUEST['price_minimum'] : '';
                                            // $wp_rem_form_fields_frontend->wp_rem_form_select_render(
                                            //     array(
                                            //         'simple' => true,
                                            //         'cust_name' => 'price_minimum',
                                            //         'std' => $price_min_checked,
                                            //         'classes' => 'chosen-select-no-single',
                                            //         'options' => $price_min,
                                            //         'extra_atr' => 'onchange="wp_rem_property_content(\'' . $property_short_counter . '\');"',
                                            //     )
                                            // );
                                            ?>
                                        </li>
                                    </ul>
                                </div>
                                <div class="select-categories">

                                    <ul>
                                        <li>
                                            <?php
                                            // $price_max_checked = ( isset($_REQUEST['price_maximum']) && $_REQUEST['price_maximum'] ) ? $_REQUEST['price_maximum'] : '';
                                            // $wp_rem_form_fields_frontend->wp_rem_form_select_render(
                                            //     array(
                                            //         'simple' => true,
                                            //         'cust_name' => 'price_maximum',
                                            //         'std' => $price_max_checked,
                                            //         'classes' => 'chosen-select-no-single',
                                            //         'options' => $price_max,
                                            //         'extra_atr' => 'onchange="wp_rem_property_content(\'' . $property_short_counter . '\');"',
                                            //     )
                                            // );
                                            ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
                    <?php } ?>
                    <?php do_action('wp_rem_property_type_fields', $property_type_slug); ?>
                    <?php do_action('wp_rem_property_type_features', $property_type_slug, $property_short_counter); ?>
                    <?php
                    $wp_rem_form_fields_frontend->wp_rem_form_hidden_render(
                        array(
                            'simple' => true,
                            'cust_id' => 'advanced_search',
                            'cust_name' => 'advanced_search',
                            'std' => 'true',
                            'classes' => '',
                        )
                    );
                    ?>
                    </div>
                    <!-- search-advanced-field-inner -->
                </div>
                <?php
            }
        }
        ?>
    </div>
</form>