<?php
/**
 * Author: Acquest.kokila
 * Date: 2/27/2020
 * Time: 8:58 AM
 * Plugin Name: Plugin Search
 * version: 1.0
 */

// create custom plugin settings menu
define('REGISTRATION_INCLUDE_URL', plugin_dir_url(__FILE__).'includes/');

//Adding frontend Styles from includes folder
function tuts_styl_incl(){
    wp_enqueue_style('styl_css_and_js', REGISTRATION_INCLUDE_URL."front-style.css");
    wp_enqueue_script('styl_css_and_js');
}

add_action('wp_footer','styl_incl');
//Adding search shortcode
//add_shortcode( 'search-box-plugin', 'plugin_search_shortcode' );

function plugin_search_shortcode($atts, $content = ""){
    ?>
    <div class="wp-rem-property-content main-search fancy v2" id="wp-rem-property-content-<?php echo esc_html($property_short_counter); ?>">

        <ul id="nav-tabs-ID" class="nav nav-tabs" role="tablist">
            <li class="list1 nav-item active"><a class="nav-link" id="searchtab1" data-toggle="tab" href="#search1"  role="tab" aria-controls="search1" aria-selected="true"><?php echo 'Residential'; ?></a></li>
            <li class="list2 nav-item"><a class="nav-link" id="searchtab2" data-toggle="tab" href="#search2"  role="tab" aria-controls="search2" aria-selected="true"><?php echo 'New Development'; ?></a></li>
            <li class="list3 nav-item"><a class="nav-link" id="searchtab3" data-toggle="tab" href="#search3"  role="tab" aria-controls="search3" aria-selected="true"><?php echo 'Commercial'; ?></a></li>
            <li class="list4 nav-item"><a class="nav-link" id="searchtab4" data-toggle="tab" href="#search4"  role="tab" aria-controls="search4" aria-selected="true"><?php echo 'Investments'; ?></a></li>
        </ul>
        <div id="Property-content" class="tab-content">
            <div class="tab-pane fade show active" id="search1" role="tabpanel" aria-labelledby="search1-tab">
                <form action="" method="get">

                </form>
            </div>
            <div class="tab-pane fade" id="search2" role="tabpanel" aria-labelledby="search2-tab">...</div>
            <div class="tab-pane fade" id="search3" role="tabpanel" aria-labelledby="search3-tab">...</div>
            <div class="tab-pane fade" id="search4" role="tabpanel" aria-labelledby="search4-tab">...</div>
        </div>
        <div class="advanced-search"><a href="javascript:void(0);" onclick="wp_rem_advanced_search_field('<?php echo esc_attr($property_short_counter); ?>', 'advance', this);"><?php echo wp_rem_plugin_text_srt('wp_rem_listsearch_advanced'); ?>&nbsp;&nbsp;&nbsp; <i class="icon-add"></i></a></div>
    </div>
<?php
}
?>
    <script>
        if (jQuery('.chosen-select, .chosen-select-deselect, .chosen-select-no-single, .chosen-select-no-results, .chosen-select-width').length != '') {
            var config = {
                '.chosen-select': {width: "100%"},
                '.chosen-select-deselect': {allow_single_deselect: true},
                '.chosen-select-no-single': {disable_search_threshold: 10, width: "100%"},
                '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
                '.chosen-select-width': {width: "95%"}
            };
            for (var selector in config) {
                jQuery(selector).chosen(config[selector]);
            }
        }
    </script>