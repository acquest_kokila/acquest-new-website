<?php
require_once("wp-load.php");

$url = 'https://www.google.com/search?q=1+USD+to+LKR';
$content = file_get_contents($url);
preg_match('/<div class="BNeawe iBp4i AP7Wnd">(.*?)<\/div>/s', $content, $match);
$currency_usd = explode( ' ' , strip_tags($match[0]));
// $usd_to_lkr = 189.005;//Get the USD to LKR price from service
$usd_to_lkr = $currency_usd[0];
$url = 'https://www.google.com/search?q=1+LKR+to+USD';
$content = file_get_contents($url);
preg_match('/<div class="BNeawe iBp4i AP7Wnd">(.*?)<\/div>/s', $content, $match);
$currency_lkr = explode( ' ' , strip_tags($match[0]));
// $lkr_to_usd = 0.00528897;//Get the LKR to USD price from service
$lkr_to_usd = $currency_lkr[0];

$wp_rem_plugin_options = get_option('wp_rem_plugin_options');
$wp_rem_plugin_options['wp_rem_usd_to_lkr'] = $usd_to_lkr;
$wp_rem_plugin_options['wp_rem_lkr_to_usd'] = $lkr_to_usd;

update_option('wp_rem_plugin_options',$wp_rem_plugin_options);

$property_object = new post_type_property();
//$property_object->create_daily_currency_exchange_rate_update_callback();
$property_object->create_daily_properties_currency_update_callback();



?>